class NotificationClassTopicInNewPhase:

    def __init__(self, db_connection, push_service):
        self.db = db_connection
        self.push = push_service
        self.to_topic = False
        self.phase_response = {
            'edit_topics': 'Diskturiere über die Ideen dort!',
            'feasibility': 'Sprich deinen Schulleiter an!',
            'vote': 'Geh abstimmen!',
            'finished': 'Schau dir die Ergebnisse an!'
        }

    def set_db_change_data(self, data):
        self.db_change = data

    def test(self, routing_key):
        if routing_key == 'row_change.table-topic.event-UPDATE' \
                and self.db_change['new_row']['idea_space'] is not None \
                and self.db_change['old_row']['phase'] is not self.db_change['new_row']['phase']:
            return True
        else:
            return False

    # Fetch firebase token from all users matching directives
    def get_message_info(self):
        self.db.execute("begin")
        self.db.execute('SET "request.jwt.claim.user_group" TO \'admin\'')
        self.db.execute('SET "request.jwt.claim.school_id" TO %s', (self.db_change['new_row']['school_id'],))


        self.db.execute("SELECT config->'firebase_id' AS firebase_id \
            FROM  aula.users \
            WHERE aula.users.school_id = %s \
            AND   aula.users.config->'firebase_id' != '\"\"' \
            AND   aula.users.config->'notifications'->'classTopicPhaseChange' = 'true' \
            AND   id in ( \
                    SELECT user_id FROM aula.user_role \
                    WHERE idea_space = %s);",
                        (self.db_change['new_row']['school_id'],
                         self.db_change['new_row']['idea_space']))

        message_info = self.db.fetchall()

        self.db.execute('SET "request.jwt.claim.user_group" TO \'\'')
        self.db.execute("end;")

        return message_info

    def send_notification(self, messages):
        # Send to multiple devices by passing a list of ids.
        for message in messages:
            self.push.notify_single_device(
                registration_id=message["firebase_id"],
                message_title="Thema in neuer Phase!",
                message_body="Das Thema {} ist jetzt in der {}!!".format(
                    self.db_change['new_row']['title'],
                    self.phase_response[self.db_change['new_row']['phase']]
                )
            )
