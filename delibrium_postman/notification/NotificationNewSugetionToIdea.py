class NotificationNewSugetionToIdea:

    def __init__(self, db_connection, push_service):
        self.db = db_connection
        self.push = push_service

    def set_db_change_data(self, data):
        self.db_change = data

    def test(self, routing_key):
        if routing_key == 'row_change.table-comment.event-INSERT':
            return True
        else:
            return False

    # Fetch firebase token from all users matching directives
    def get_message_info(self):
        self.db.execute("begin")
        self.db.execute('SET "request.jwt.claim.user_group" TO \'admin\'')
        self.db.execute('SET "request.jwt.claim.school_id" TO %s', (self.db_change['row']['school_id'],))

        self.db.execute("SELECT \
                      u1.config->'firebase_id' AS firebase_id, \
                      u1.id AS user_id, \
                      u2.id AS comment_id, \
                      u2.username AS username, \
                      aula.idea.title AS title, \
                      idea_space \
                FROM  aula.comment \
                JOIN  aula.idea ON aula.idea.id = aula.comment.parent_idea \
                    LEFT JOIN aula.users AS u1 ON aula.idea.created_by = u1.id \
                    LEFT JOIN aula.users AS u2 ON aula.comment.created_by = u2.id \
                WHERE aula.comment.id = %s \
                AND   u1.config->'firebase_id' != '\"\"';",
                        (self.db_change['row']['id'],))

        message_info = self.db.fetchone()

        self.db.execute('SET "request.jwt.claim.user_group" TO \'\'')
        self.db.execute("end;")

        return message_info

    def get_idea_space(self, idea_space):
        self.db.execute("begin")

        self.db.execute('SET "request.jwt.claim.user_group" TO \'admin\'')
        self.db.execute('SET "request.jwt.claim.school_id" TO %s', (self.db_change['row']['school_id'],))

        self.db.execute("SELECT slug FROM aula.idea_space \
                WHERE school_id = %s \
                AND id = %s;",
                        (self.db_change['row']['school_id'],
                         idea_space,
                         ))

        idea_space = self.db.fetchone()

        self.db.execute('SET "request.jwt.claim.user_group" TO \'\'')
        self.db.execute("end;")

        return idea_space

    def send_notification(self, messages):
        # Send to multiple devices by passing a list of ids.
        if messages['user_id'] is not messages['comment_id']:
            idea_space_slug = "school"
            if messages['idea_space'] is not None:
                idea_space_slug = self.get_idea_space(messages['idea_space'])

            self.push.notify_single_device(
                registration_id=messages['firebase_id'],
                message_title="Mach deine Idee besser!",
                message_body="Zu deiner Idee {} gibt es einen neuen Verbesserungsvorschlag von {}".format(
                    messages['title'],
                    messages['username']
                ),
                data_message={
                    "url": "/space/{}/idea/{}/view".format(
                        idea_space_slug,
                        self.db_change['row']['parent_idea']
                    )
                }
            )
