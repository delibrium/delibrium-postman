class NotificationSchoolNewIdea:

    def __init__(self, db_connection, push_service):
        self.db = db_connection
        self.push = push_service

    def set_db_change_data(self, data):
        self.db_change = data

    def test(self, routing_key):
        if routing_key == 'row_change.table-idea.event-INSERT' \
                and self.db_change['row']['idea_space'] is None:
            return True
        else:
            return False

    def get_message_info(self):
        self.db.execute("begin")
        self.db.execute('SET "request.jwt.claim.user_group" TO \'admin\'')
        self.db.execute('SET "request.jwt.claim.school_id" TO %s', (self.db_change['row']['school_id'],))

        self.db.execute("SELECT \
                      config->'firebase_id' AS firebase_id \
                FROM  aula.users \
                WHERE school_id = %s \
                AND   aula.users.config->'firebase_id' != '\"\"' \
                AND   aula.users.config->'notifications'->'newSchoolIdea' = 'true' \
                AND   id != %s;",
                        (self.db_change['row']['school_id'],
                         self.db_change['row']['created_by']))

        tokens = self.db.fetchall()

        self.db.execute("SELECT \
                      username  \
                FROM  aula.users \
                WHERE school_id = %s \
                AND   id = %s;",
                        (self.db_change['row']['school_id'],
                         self.db_change['row']['created_by']))

        msgOwner = self.db.fetchone()
        print('OWNER',self.db_change['row']['school_id'],self.db_change['row']['created_by'], msgOwner)

        self.db.execute('SET "request.jwt.claim.user_group" TO \'\'')
        self.db.execute("end;")

        message_info = {'msgOwner': msgOwner, 'tokens': tokens}

        return message_info

    def send_notification(self, messages):
        # Send to multiple devices by passing a list of ids.
        users = [token['firebase_id'] for token in messages['tokens']]
        creator = messages['msgOwner']

        print("USERS => ",users)

        r = self.push.notify_multiple_devices(
            registration_ids=users,
            message_title="Eine neue Idee!",
            message_body="{} hat die Idee: {}".format(
                creator['username'],
                self.db_change['row']['title']
            ),
            data_message={
                "url": "/space/school/idea/{}/view".format(
                    self.db_change['row']['id']
                )
            }
        )

        print("ERROR", r)
