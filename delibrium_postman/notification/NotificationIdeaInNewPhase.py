class NotificationIdeaInNewPhase:

    def __init__(self, db_connection, push_service):
        self.db = db_connection
        self.push = push_service
        self.to_topic = False
        self.phase_response = {
            'edit_topics': 'Arbeite die Verbesserungsvorschläge ein!',
            'feasibility': 'Sprich deinen Schulleiter an!',
            'vote': 'Stimm über sie ab und mach Werbung!',
            'finished': 'Schau, wie sie abgeschnitten hat!'
        }

    def set_db_change_data(self, data):
        self.db_change = data

    def test(self, routing_key):
        if routing_key == 'row_change.table-idea.event-UPDATE' \
                and self.db_change['old_row']['topic'] is None \
                and self.db_change['new_row']['topic'] is not None:
            self.to_topic = True
            return True
        elif routing_key == 'row_change.table-topic.event-UPDATE' \
                and self.db_change['old_row']['phase'] is not self.db_change['new_row']['phase']:
            return True
        else:
            return False

    # Fetch firebase token from all users matching directives
    def get_message_info(self):
        self.db.execute("begin")
        self.db.execute('SET "request.jwt.claim.user_group" TO \'admin\'')
        self.db.execute('SET "request.jwt.claim.school_id" TO %s', (self.db_change['new_row']['school_id'],))

        if self.to_topic:
            self.db.execute("SELECT aula.users.config->'firebase_id' AS firebase_id, \
                      aula.idea.title AS title, phase \
                FROM  aula.users \
                JOIN  aula.idea ON aula.users.id = aula.idea.created_by \
                JOIN  aula.topic ON aula.idea.topic = aula.topic.id \
                WHERE aula.users.school_id = %s \
                AND   aula.users.config->'firebase_id' IS NOT NULL \
                AND   aula.users.config->'firebase_id' != '\"\"' \
                AND   aula.users.config->'notifications'->'ideaPhaseChange' = 'true' \
                AND   aula.idea.id = %s;",
                            (self.db_change['new_row']['school_id'],
                             self.db_change['new_row']['id']))
        else:
            self.db.execute("SELECT aula.users.config->'firebase_id' AS firebase_id, \
                      aula.idea.title AS title, phase \
                FROM  aula.users \
                JOIN  aula.idea ON aula.users.id = aula.idea.created_by \
                JOIN  aula.topic ON aula.idea.topic = aula.topic.id \
                WHERE aula.users.school_id = %s \
                AND   aula.users.config->'firebase_id' IS NOT NULL \
                AND   aula.users.config->'firebase_id' != '\"\"' \
                AND   aula.users.config->'notifications'->'ideaPhaseChange' = 'true' \
                AND   aula.idea.topic = %s;",
                            (self.db_change['new_row']['school_id'],
                             self.db_change['new_row']['id'],))

        message_info = self.db.fetchall()

        self.db.execute('SET "request.jwt.claim.user_group" TO \'\'')
        self.db.execute("end;")

        return message_info

    def send_notification(self, messages):
        # Send to multiple devices by passing a list of ids.
        for message in messages:

            self.push.notify_single_device(
                registration_id=message['firebase_id'],
                message_title="Idee in neuer Phase",
                message_body="Deine Idee {} ist jetzt in der {}!!".format(
                    message['title'],
                    self.phase_response[message['phase']]
                ),
            )
