class NotificationClassNewIdea:

    def __init__(self, db_connection, push_service):
        self.db = db_connection
        self.push = push_service

    def set_db_change_data(self, data):
        self.db_change = data

    def test(self, routing_key):
        if routing_key == 'row_change.table-idea.event-INSERT' \
                and self.db_change['row']['idea_space'] is not None:
            return True
        else:
            return False

    # Fetch firebase token from all users matching directives
    def get_message_info(self):
        self.db.execute("begin")
        self.db.execute('SET "request.jwt.claim.user_group" TO \'admin\'')
        self.db.execute('SET "request.jwt.claim.user_id" TO 1')
        self.db.execute('SET "request.jwt.claim.school_id" TO %s', (self.db_change['row']['school_id'],))


        self.db.execute("SELECT \
                      config->'firebase_id' AS firebase_id \
                FROM  aula.users \
                WHERE school_id = %s \
                AND   aula.users.config->'firebase_id' != '\"\"' \
                AND   aula.users.config->'notifications'->'newClassIdea' = 'true' \
                AND   id != %s \
                AND   id in ( \
                    SELECT user_id FROM aula.user_role \
                    WHERE idea_space = %s);",
                        (self.db_change['row']['school_id'],
                         self.db_change['row']['created_by'],
                         self.db_change['row']['idea_space']))

        tokens = self.db.fetchall()

        self.db.execute("SELECT \
                      username \
                FROM  aula.users \
                WHERE school_id = %s \
                AND   id = %s;",
                        (self.db_change['row']['school_id'],
                         self.db_change['row']['created_by']))

        msgOwner = self.db.fetchone()

        self.db.execute("SELECT slug FROM aula.idea_space \
                WHERE school_id = %s \
                AND id = %s;",
                        (self.db_change['row']['school_id'],
                         self.db_change['row']['idea_space'],
                         ))

        ideaSpace = self.db.fetchone()

        self.db.execute('SET "request.jwt.claim.user_group" TO \'\'')
        self.db.execute("end;")

        message_info = {'msgOwner': msgOwner,
                        'tokens': tokens,
                        'ideaSpace': ideaSpace}

        return message_info

    def send_notification(self, messages):
        # Send to multiple devices by passing a list of ids.
        users = [token['firebase_id'] for token in messages['tokens']]
        creator = messages['msgOwner']
        idea_space_slug = messages['ideaSpace']['slug']

        self.push.notify_multiple_devices(
            registration_ids=users,
            message_title="Eine neue Idee!",
            message_body="{} hat die Idee: {}".format(
                creator['username'],
                self.db_change['row']['title']
            ),
            data_message={
                "url": "/space/{}/idea/{}/view".format(
                    idea_space_slug,
                    self.db_change['row']['id']
                )
            }
        )
