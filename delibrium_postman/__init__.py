import os
import json
import logging
from pyfcm import FCMNotification
from pyfcm.errors import FCMError, FCMServerError, InvalidDataError
import pika
import psycopg2
import psycopg2.extras
from dotenv import load_dotenv
from .notification import NotificationSchoolNewIdea, \
    NotificationClassNewIdea, \
    NotificationNewSugetionToIdea, \
    NotificationIdeaInNewPhase, \
    NotificationSchoolTopicInNewPhase, \
    NotificationClassTopicInNewPhase

load_dotenv()


class DelibriumPostman:

    notification_managers = []

    def __init__(self):
        self.connect_rabbitmq()
        self.connect_db()
        self.set_firebase()
        self.set_notification_managers()
        self.channel.start_consuming()

    def set_notification_managers(self):
        print("Configure Notifications Managers")
        notification_school_new_idea = NotificationSchoolNewIdea(
            self.db_cur, self.push)
        self.notification_managers.append(notification_school_new_idea)

        notification_class_new_idea = NotificationClassNewIdea(
            self.db_cur, self.push)
        self.notification_managers.append(notification_class_new_idea)

        notification_sugestion_to_idea = NotificationNewSugetionToIdea(
            self.db_cur, self.push)
        self.notification_managers.append(notification_sugestion_to_idea)

        notification_idea_new_phase = NotificationIdeaInNewPhase(
            self.db_cur, self.push)
        self.notification_managers.append(notification_idea_new_phase)

        notification_school_topic_new_phase = NotificationSchoolTopicInNewPhase(
            self.db_cur, self.push)
        self.notification_managers.append(notification_school_topic_new_phase)

        notification_class_topic_new_phase = NotificationClassTopicInNewPhase(
            self.db_cur, self.push)
        self.notification_managers.append(notification_class_topic_new_phase)

    def connect_rabbitmq(self):
        print("Connect with RabbitMQ")
        self.rabbitmq_conn = pika.BlockingConnection(
            pika.ConnectionParameters(host='localhost'))
        self.channel = self.rabbitmq_conn.channel()

        self.channel.exchange_declare(
            'aula',
            exchange_type='fanout'
        )

        self.channel.queue_declare(queue='test')
        self.channel.queue_bind(exchange='aula', queue='test')
        self.channel.basic_consume(
            queue='test', on_message_callback=self.rabbitmq_callback, auto_ack=True)

    def rabbitmq_callback(self, ch, method, properties, body):
        rmq_response = json.loads(body)
        print(rmq_response)

        for nm in self.notification_managers:
            nm.set_db_change_data(rmq_response)

            if nm.test(method.routing_key):
                message = nm.get_message_info()
                if message is not None and len(message) > 0:
                    try:
                        nm.send_notification(message)
                    except FCMError:
                        logging.error(" FCMError: {}, {}, {}, {}, {}".format(
                            ch, method, properties, body, message))
                    except FCMServerError:
                        logging.error(" FCMServerError: {}, {}, {}, {}, {}".format(
                            ch, method, properties, body, message))
                    except InvalidDataError:
                        logging.error(" InvalidDataError: {}, {}, {}, {}, {}".format(
                            ch, method, properties, body, message))
                    except:
                        logging.error(" Error sending message to firebase: {}, {}, {}, {}, {}".format(
                            ch, method, properties, body, message))

    def connect_db(self):
        print("Connect with Postgres")
        # Connect to an existing database
        self.db_conn = psycopg2.connect(
            dbname=os.getenv("AULA_DB_NAME"),
            user=os.getenv("AULA_DB_USER"),
            password=os.getenv("AULA_DB_PASS"),
            host=os.getenv("AULA_DB_HOST"),
            port=int(os.getenv("AULA_DB_PORT"))
        )

        # Open a cursor to perform database operations
        self.db_cur = self.db_conn.cursor(
            cursor_factory=psycopg2.extras.RealDictCursor)

    def set_firebase(self):
        self.push = push_service = FCMNotification(
            api_key=os.getenv("FIREBASE_API_KEY"))


def start_delibrium_postman():
    postman = DelibriumPostman()
